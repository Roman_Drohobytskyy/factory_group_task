package com.group4.controller;

import com.group4.model.bakery.Bakery;
import com.group4.model.bakery.BakeryCity;
import com.group4.model.bakery.BakeryFactory;
import com.group4.model.pizza.Ingredient;
import com.group4.model.pizza.Pizza;
import com.group4.model.pizza.PizzaSize;
import com.group4.model.pizza.PizzaType;
import com.group4.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    private Pattern orderPizzaPattern = Pattern.compile("(?i)order\\s+(\\w+)\\s+(\\w+)");
    private Pattern orderCustomPizzaPattern = Pattern.compile("(?i)order\\s+custom\\s+pizza\\s+(\\w+)");
    private Pattern selectCityPattern = Pattern.compile("(?i)select\\s+city\\s+(\\w+)");
    private Pattern ingredientsPizzaTypePattern = Pattern.compile("(?i)get\\s+ingredients\\s+(\\w+)");
    private Pattern pricePizzaTypePattern = Pattern.compile("(?i)get\\s+price\\s+(\\w+)");
    private Pattern pizzaMenuPattern = Pattern.compile("(?i)get\\s+menu");
    private Pattern manualPattern = Pattern.compile("(?i)manual");
    private Pattern exitPattern = Pattern.compile("(?i)exit");

    private static Logger logger = LogManager.getLogger(Controller.class);
    private Bakery bakery = BakeryFactory.createBakery(BakeryCity.LVIV);

    public Printable execute(View view, String command) throws IllegalArgumentException {
        Printable message = null;
        if (command.matches(manualPattern.pattern())) {
            message = () -> logger.trace(readManual());
        } else if (command.matches(exitPattern.pattern())) {
            message = () -> logger.trace("Bye.");
        } else if (command.matches(pizzaMenuPattern.pattern())) {
            message = () -> {
                logger.trace("Pizza:\n");
                logger.trace(Arrays.asList(PizzaType.values()) + "\n");
                logger.trace("Size:\n");
                logger.trace(Arrays.asList(PizzaSize.values()) + "\n");
            };
        } else if (command.matches(selectCityPattern.pattern())) {
            Matcher matcher = selectCityPattern.matcher(command);
            if (matcher.find()) {
                bakery = BakeryFactory.createBakery(BakeryCity.valueOf(matcher.group(1).toUpperCase()));
                message = () -> logger.trace("Done.\n");
            }
        } else if (command.matches(ingredientsPizzaTypePattern.pattern())) {
            Matcher matcher = ingredientsPizzaTypePattern.matcher(command);
            if (matcher.find()) {
                message = () -> logger.trace(PizzaType.valueOf(matcher.group(1)
                        .toUpperCase())
                        .getIngredients()
                        + "\n"
                );
            }
        } else if (command.matches(pricePizzaTypePattern.pattern())) {
            Matcher matcher = pricePizzaTypePattern.matcher(command);
            if (matcher.find()) {
                message = () -> {
                    PizzaType type = PizzaType.valueOf(matcher.group(1)
                            .toUpperCase());
                    for (PizzaSize size : PizzaSize.values()) {
                        logger.trace(type + " " + size + " "
                                + (type.getPizzaTypePrice() + size.getPrice())
                                + " UAH\n");
                    }
                };
            }
        } else if (command.matches(orderPizzaPattern.pattern())) {
            Matcher matcher = orderPizzaPattern.matcher(command);
            if (matcher.find()) {
                boolean here = view.askUser("Here?");
                Pizza pizza = bakery.bakePizza(
                        PizzaType.valueOf(matcher.group(1).toUpperCase()),
                        here,
                        PizzaSize.valueOf(matcher.group(2).toUpperCase())
                );
                message = () -> logger.trace(pizza + "\n");
            }
        } else if (command.matches(orderCustomPizzaPattern.pattern())) {
            Matcher matcher = orderCustomPizzaPattern.matcher(command);
            if (matcher.find()) {
                List<Ingredient> ingredients = view.enterIngredients();
                boolean here = view.askUser("Here?");
                Pizza pizza = bakery.bakePizza(
                        here,
                        PizzaSize.valueOf(matcher.group(1).toUpperCase()),
                        ingredients
                );
                message = () -> logger.trace(pizza + "\n");
            }
        } else {
            message = () -> logger.error("Illegal command. Repeat please.\n");
        }

        return message;
    }

    private String readManual() {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    "src/main/resources/manual.txt"));
            String line = reader.readLine();
            while (line != null) {
                stringBuilder.append(line);
                stringBuilder.append('\n');
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            logger.error(e);
        }
        return stringBuilder.toString();
    }
}
