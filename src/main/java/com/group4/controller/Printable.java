package com.group4.controller;

@FunctionalInterface
public interface Printable {
    void print();
}
