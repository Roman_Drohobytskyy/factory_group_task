package com.group4.view;

import com.group4.controller.Controller;
import com.group4.model.pizza.Ingredient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class View {
    private static Logger logger = LogManager.getLogger(View.class);
    private Controller controller;
    public View(final Controller controller) {
        this.controller = controller;
    }
    public void initUI() {
        Scanner scanner = new Scanner(System.in);
        String command;
        logger.trace("Make your order, please.\n");
        logger.trace("(Enter \"manual\" in order to get acquainted)\n");
        do {
            logger.trace("\n>> ");
            command = scanner.nextLine().trim();
            try {
                controller.execute(this, command).print();
            } catch (IllegalArgumentException e) {
                logger.error(e);
            }
        } while (!command.matches("(?i)exit"));
    }

    public boolean askUser(String question) {
        logger.trace(question + "\n");
        Scanner scanner = new Scanner(System.in);
        String answer;
        do {
            logger.trace("\nyes/no\n>> ");
            answer = scanner.nextLine().trim();
        } while (!answer.matches("(?i)yes|no"));
        return answer.matches("(?i)yes");
    }

    public List<Ingredient> enterIngredients() {
        logger.trace("Enter ingredients until empty line.\n");
        logger.trace(Arrays.asList(Ingredient.values()));
        Scanner scanner = new Scanner(System.in);
        List<Ingredient> ingredients = new ArrayList<>();
        String sizeName;
        do {
            logger.trace("\ningredient: ");
            sizeName = scanner.nextLine().trim();
            try {
                ingredients.add(Ingredient.valueOf(sizeName.replace(' ','_').toUpperCase()));
                logger.trace("Added.");
            } catch (IllegalArgumentException e) {
                if (!sizeName.isEmpty()) {
                    logger.error("Illegal ingredient.");
                }
            }
        } while (!sizeName.isEmpty());

        return ingredients.stream()
                .distinct()
                .collect(Collectors.toList());
    }
}
