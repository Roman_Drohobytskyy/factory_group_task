package com.group4.model.bakery;

import com.group4.model.bakery.bakeryImpl.DniproBakery;
import com.group4.model.bakery.bakeryImpl.KyivBakery;
import com.group4.model.bakery.bakeryImpl.LvivBakery;

public class BakeryFactory {
    public static Bakery createBakery(BakeryCity bakeryCity) {
        if(BakeryCity.DNIPRO == bakeryCity) {
            return new DniproBakery();
        } else if(bakeryCity == BakeryCity.LVIV) {
            return new LvivBakery();
        } else if (bakeryCity == BakeryCity.KYIV) {
            return new KyivBakery();
        }
        else return null;
    }
}
