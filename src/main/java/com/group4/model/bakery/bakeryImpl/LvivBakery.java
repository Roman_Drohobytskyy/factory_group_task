package com.group4.model.bakery.bakeryImpl;

import com.group4.model.pizza.Ingredient;
import com.group4.model.pizza.Pizza;
import com.group4.model.pizza.PizzaSize;
import com.group4.model.pizza.PizzaType;
import com.group4.model.bakery.Bakery;
import com.group4.model.pizza.pizzaImpl.*;

import java.util.List;

public class LvivBakery extends Bakery {

    protected Pizza createPizza(PizzaType pizzaType, PizzaSize pizzaSize) {
        return new LvivPizza(pizzaType, pizzaSize);
    }

    @Override
    protected Pizza createPizza(PizzaSize pizzaSize, List<Ingredient> ingredients) {
        return new LvivPizza(pizzaSize, ingredients);
    }
}
