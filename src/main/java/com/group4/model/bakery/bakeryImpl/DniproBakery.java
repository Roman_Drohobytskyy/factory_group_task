package com.group4.model.bakery.bakeryImpl;

import com.group4.model.pizza.Ingredient;
import com.group4.model.pizza.Pizza;
import com.group4.model.pizza.PizzaSize;
import com.group4.model.pizza.PizzaType;
import com.group4.model.bakery.Bakery;
import com.group4.model.pizza.pizzaImpl.DniproPizza;

import java.util.List;

public class DniproBakery extends Bakery {

    protected Pizza createPizza(PizzaType pizzaType, PizzaSize pizzaSize) {
        return new DniproPizza(pizzaType, pizzaSize);
    }

    @Override
    protected Pizza createPizza(PizzaSize pizzaSize, List<Ingredient> ingredients) {
        return new DniproPizza(pizzaSize, ingredients);
    }
}
