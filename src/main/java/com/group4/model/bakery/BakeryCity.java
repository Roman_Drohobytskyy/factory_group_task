package com.group4.model.bakery;

public enum BakeryCity {
    LVIV,
    KYIV,
    DNIPRO
}
