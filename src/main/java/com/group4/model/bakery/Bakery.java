package com.group4.model.bakery;

import com.group4.model.pizza.Ingredient;
import com.group4.model.pizza.Pizza;
import com.group4.model.pizza.PizzaSize;
import com.group4.model.pizza.PizzaType;

import java.util.List;

public abstract class Bakery {

    protected abstract Pizza createPizza(PizzaType pizzaType, PizzaSize pizzaSize);
    protected abstract Pizza createPizza(PizzaSize pizzaSize, List<Ingredient> ingredients);

    public Pizza bakePizza (PizzaType pizzaType, boolean here, PizzaSize pizzaSize) {
        Pizza pizza = createPizza(pizzaType, pizzaSize);
        prepareOrder(pizza, here);
        return pizza;
    }

    public Pizza bakePizza(boolean here, PizzaSize pizzaSize, List<Ingredient> ingredients) {
        Pizza pizza = createPizza(pizzaSize, ingredients);
        prepareOrder(pizza, here);
        return pizza;
    }

    private void prepareOrder(Pizza pizza, boolean here) {
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box(here);
    }
}
