package com.group4.model.pizza;

public enum PizzaSize {
    SMALL(28, 305, 26),
    MEDIUM(40, 580, 45);

    private final int price;
    private final int diameter;
    private final int weight;

    PizzaSize(int diameter, int weight, int price) {
        this.diameter = diameter;
        this.weight = weight;
        this.price = price;
    }

    public int getDiameter() {
        return diameter;
    }

    public int getWeight() {
        return weight;
    }

    public int getPrice() {
        return price;
    }


    @Override
    public String toString() {
        return this.name().substring(0, 1).toUpperCase() +
                this.name().substring(1).toLowerCase()
                        .replace('_', ' ');
    }
}
