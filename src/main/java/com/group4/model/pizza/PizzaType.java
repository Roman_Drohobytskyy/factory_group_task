package com.group4.model.pizza;

import java.util.Arrays;
import java.util.List;

public enum PizzaType {
    CAESAR(Arrays.asList(
            Ingredient.BRANDED_TOMATO_SAUCE,
            Ingredient.CHEESE_MOZZARELLA,
            Ingredient.BACON,
            Ingredient.CHEESE_PARMESAN,
            Ingredient.CHICKEN_MEET,
            Ingredient.SALAD)),
    BAVARIA(Arrays.asList(
            Ingredient.BRANDED_TOMATO_SAUCE,
            Ingredient.CHEESE_MOZZARELLA,
            Ingredient.SAUSAGES,
            Ingredient.MEET,
            Ingredient.FRESH_MUSHROOMS,
            Ingredient.TOMATOES,
            Ingredient.PARSLEY)),
    POLLO_AMERICANO(Arrays.asList(
            Ingredient.BRANDED_TOMATO_SAUCE,
            Ingredient.CHEESE_MOZZARELLA,
            Ingredient.OLIVES,
            Ingredient.CHICKEN_MEET,
            Ingredient.BACON,
            Ingredient.PEPPERONI_PEPPER)),
    HAWAIIAN(Arrays.asList(
            Ingredient.BRANDED_TOMATO_SAUCE,
            Ingredient.CHEESE_MOZZARELLA,
            Ingredient.CHICKEN_MEET,
            Ingredient.PINEAPPLE,
            Ingredient.BASIL)),
    QUATTRO_FORMAGGI(Arrays.asList(
            Ingredient.BRANDED_TOMATO_SAUCE,
            Ingredient.CHEESE_MOZZARELLA,
            Ingredient.CHEESE_GAUDA,
            Ingredient.CHEESE_DOR_BLUE,
            Ingredient.CHEESE_RADOMER)),
    DIABOLA(Arrays.asList(
            Ingredient.BRANDED_TOMATO_SAUCE,
            Ingredient.CHEESE_MOZZARELLA,
            Ingredient.SALAMI,
            Ingredient.CHILI_PEPPER,
            Ingredient.PEPPER,
            Ingredient.OLIVES)),
    USERS_PIZZA(null);

    private List<Ingredient> ingredients;

    PizzaType(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public int getPizzaTypePrice() {
        int price = 0;
        for (Ingredient i : this.ingredients) {
            price += i.getPrice();
        }
        return price;
    }


    @Override
    public String toString() {
        return this.name().substring(0, 1).toUpperCase() +
                this.name().substring(1).toLowerCase()
                        .replace('_', ' ');
    }
}
