package com.group4.model.pizza;

public enum Ingredient {
    ARTICHOKES(19),
    BACON(28),
    BRANDED_TOMATO_SAUCE(1),
    BASIL(9),
    CHEESE_DOR_BLUE(30),
    CHEESE_GAUDA(22),
    CHEESE_FETA(20),
    CHEESE_MOZZARELLA(20),
    CHEESE_MOZZARELLA_FRESH(24),
    CHEESE_RADOMER(27),
    CHEESE_PARMESAN(24),
    CHICKEN_MEET(23),
    CHILI_PEPPER(13),
    GREEN_SALAD(13),
    FRESH_MUSHROOMS(14),
    FRIED_MUSHROOMS(13),
    MEET(23),
    MUSSELS(30),
    PARSLEY(4),
    PEPPER(13),
    PEPPERONI_PEPPER(14),
    PINEAPPLE(13),
    PRAWN(43),
    PROSHUTTO(33),
    OLIVES(14),
    ONIONS(7),
    SALAD(17),
    SALAMI(23),
    SAUSAGES(27),
    SALMON(44),
    TUNA(26),
    TOMATOES(14);

    private int price;

    Ingredient(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }


    @Override
    public String toString() {
        return this.name().substring(0, 1).toUpperCase() +
            this.name().substring(1).toLowerCase()
                    .replace('_', ' ');
    }
}
