package com.group4.model.pizza;

public interface Pizza {
    void prepare();

    void bake();

    void cut();

    void box(boolean here);
}
