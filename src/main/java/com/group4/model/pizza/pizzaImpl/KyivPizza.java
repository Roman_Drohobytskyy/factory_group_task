package com.group4.model.pizza.pizzaImpl;

import com.group4.model.pizza.Ingredient;
import com.group4.model.pizza.Pizza;
import com.group4.model.pizza.PizzaSize;
import com.group4.model.pizza.PizzaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class KyivPizza implements Pizza {

    private static Logger logger = LogManager.getLogger(KyivPizza.class);


    private PizzaType pizzaType;
    private final String recipe = "Recipes for pizza in Kyiv.";
    private PizzaSize pizzaSize;
    private int price;
    private int boxPrice = 5;
    private double coefficient = 1.3;

    public KyivPizza(PizzaType pizzaType, PizzaSize pizzaSize) {
        this.pizzaType = pizzaType;
        this.pizzaSize = pizzaSize;
        this.price = (int) (coefficient * (pizzaType.getPizzaTypePrice() + pizzaSize.getPrice()));
    }

    public KyivPizza(PizzaSize pizzaSize, List<Ingredient> ingredients) {
        this.pizzaType = PizzaType.USERS_PIZZA;
        pizzaType.setIngredients(ingredients);
        this.pizzaSize = pizzaSize;
        this.price = (int) (coefficient * (pizzaType.getPizzaTypePrice() + pizzaSize.getPrice()));
    }

    public void prepare() {
        logger.trace("1. Preparing your " + pizzaType + " " + pizzaSize + " pizza. " +
                "We have our own recipes: " + recipe + "\n");
    }

    public void bake() {
        logger.trace("2. Baking your " + pizzaType + " " + pizzaSize + " pizza.\n");

    }

    public void cut() {
        logger.trace("3. Cutting your " + pizzaType + " " + pizzaSize + " pizza with special knife for Pizza in Kyiv.\n");

    }

    public void box(boolean here) {
        if (!here) {
            logger.trace("4. Putting your " + pizzaType + " " + pizzaSize + " pizza in the box with logo \"Kyiv Bakery\".\n");
            price += boxPrice;
        }
    }

    @Override
    public String toString() {
        return  "\n✓  Order details -> pizza: " + pizzaType + ", size: " + pizzaSize + "\n" +
                "                    weight = "+ pizzaSize.getWeight() + " gr, " +
                "diameter = " + pizzaSize.getDiameter() + " cm." + "\n" +
                "✓  Ingredients   -> " + pizzaType.getIngredients() + "\n" +
                "✓  Total price   -> " + price + " UAH";
    }
}
