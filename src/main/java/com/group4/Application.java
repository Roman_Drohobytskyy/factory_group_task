package com.group4;

import com.group4.controller.Controller;
import com.group4.view.View;

public class Application {
    public static void main(String[] args) {
        new View(new Controller()).initUI();
    }
}
